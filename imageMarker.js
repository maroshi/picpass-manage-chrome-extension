// Unique ID for the className.
var MOUSE_VISITED_CLASSNAME = 'crx_mouse_visited';
var _webServiceUrl = 'https://localhost:8443/api/';

// Previous dom, that we want to track, so we can remove the previous styling.
var prevDOM = null;

var activeLink = null;
var activeImg = null;
var capturedImg = null;

// Add keywords list place holder element
$('body').append("<div id='keywords_placeholder'><ol id='keywords_list'></ol></div>");

// Add radiobuttons hardcoded styles
const css = document.createElement('style');
css.textContent = `
.ui-icon-blank {
    border-width: 8px !important;
    border-color: aqua  !important;
}`;
document.head.appendChild(css);

// Add popup dialog box inside keywords list place holder
$('#keywords_placeholder').append(`
<div id="dialog-1" class="imagemarker-dialog-body" title="Keyworded Images Capture">
    <p>
        <a id="img_href" target="_blank" href=https://unsplash.com/photos/2slBHG3HtdA>
            <img id="img_src" src="http://icons.iconarchive.com/icons/everaldo/crystal-clear/128/App-ksmiletris-smiley-icon.png"/>
        </a>
        <span id="img_title" Image Title</span>
    </p>
</div>
`);
$('#dialog-1').append(`
<form name="form1" class="imagemarker-dialog-form">
    <header>
        <div>Keyword</div>
        <div class="radio-header">
            <div>Noun</div>
            <div>Descr</div>
            <div>Verb</div>
        </div>
        <div>#</div>
        <div>Related</div>
     </header>
    <section class="imagemarker-dialog-section">
        <div class="keyword-text-cell">
            <input type="text" id="text1-1" class="keyword-textbox" name="text1" size="22" />
            <label for="text1-1"></label>
        </div>
        <div id="radioset1" class="radioset keyword-category-cell">
            <input type="radio" id="radio1-1" name="radio1" />
            <label for="radio1-1"></label>
            <input type="radio" id="radio1-2" name="radio1" />
            <label for="radio1-2"></label>
            <input type="radio" id="radio1-3" name="radio1" />
            <label for="radio1-3"></label>
        </div>
        <div id="keyword-count-1" class="pics-count">9</div>
        <div class="related-keywords">div 1.4</div>
    </section>
</form>
`);

var maxRowsCount = 6;
var availableKeywordsArr = [];
var allKeywordsObjArr = [];
var submitedKeywordsObjArr = [];
$(".radioset").buttonset();

// Read keywords from database into availableKeywordsArr
/* chrome.storage.local.get(['keywordCountedObjsArr'], function(result) {
    console.dir(["Read keywordCountedObjsArr", keywordCountedObjsArr]);
    keywordCountedObjsArr.forEach(function (element) {
        // console.log(element.name);
        availableKeywordsArr.push(element.name);
    });
    allKeywordsObjArr = keywordCountedObjsArr;
}); */


// Load keywors from local storage
chrome.storage.local.get(['keywordCountedObjsArr'], function (results) {
    // console.dir(["Read keywordCountedObjsArr", results]);
    keywordCountedObjsArr = results.keywordCountedObjsArr;
    keywordCountedObjsArr.forEach(function (element) {
        // console.log(element.name);
        availableKeywordsArr.push(element.name);
    });
    allKeywordsObjArr = keywordCountedObjsArr;
});
/* $.getJSON('${_webServiceUrl}keywordPicsCounts/search/all',
    function (respData) {
        let keywordCountedObjsArr = respData._embedded.keywordPicsCounts;
        // console.dir(keywordCountedObjsArr);
        keywordCountedObjsArr.forEach(function (element) {
            // console.log(element.name);
            availableKeywordsArr.push(element.name);
        });
        allKeywordsObjArr = keywordCountedObjsArr;
    }); */
// Load previous submited keywords list
chrome.storage.local.get(['submitedKeywordsObjArr'], function (results) {
    // console.dir(["Read submitedKeywordsObjArr", results]);
    submitedKeywordsObjArr = results.submitedKeywordsObjArr;
});

function sucessfullPost(msg) {
    // console.log("success : ");
    // console.dir(msg);

    // Update local keywords arrays with latest entries
    submitedKeywordsObjArr.forEach(function (keywordObj) {
        let keywordIdx = availableKeywordsArr.indexOf(keywordObj.name);
        if (keywordIdx < 0) {
            // new keyword! not found in local array. Append to local arrays
            availableKeywordsArr.push(keywordObj.name);
            allKeywordsObjArr.push(keywordObj);
            keywordIdx = allKeywordsObjArr.length - 1;
        }
        allKeywordsObjArr[keywordIdx].picsCount++;
        keywordObj.picsCount = allKeywordsObjArr[keywordIdx].picsCount;
    });
    $("#dialog-1").dialog("close");

    $.getJSON(`${_webServiceUrl}keywordPicsCounts/search/all`,
        function (respData) {
            let keywordCountedObjsArr = respData._embedded.keywordPicsCounts;
            // console.dir(keywordCountedObjsArr);
            chrome.storage.local.set({ "keywordCountedObjsArr": keywordCountedObjsArr }, function () {
                console.dir(["Saved keywordCountedObjsArr", keywordCountedObjsArr]);
            });
        });
}

$(function dialogWraper() {
    $("#dialog-1").dialog({
        autoOpen: false,
        width: 600,
        buttons: {
            "Submit image and keywords": function () {
                // Collect keywords
                let rowsArr = $(".imagemarker-dialog-section");
                let submitObj = {
                    "name": capturedImg.substr(capturedImg.lastIndexOf('?') + 1),
                    "width": $('#img_src')[0].clientWidth,
                    "height": $('#img_src')[0].clientHeight,
                    "keywords": [],
                    "pictureFileUrl": capturedImg
                }
                rowsArr.each(function (i) {
                    let row = rowsArr[i];
                    let keywordText = row.getElementsByTagName('INPUT')[0].value;
                    // console.log('text:' + keywordText);

                    // continue to next line on empty text
                    if (keywordText.length !== 0) {

                        // Read the selected label suffix (one letter)
                        let selectedCategoryLabel = row.getElementsByClassName('ui-checkboxradio-checked');
                        (selectedCategoryLabel.length == 0) ?
                            selectedCategoryLabel = '0' :
                            selectedCategoryLabel = selectedCategoryLabel[0]["htmlFor"].slice(-1);

                        // Calculate category from label suffix
                        let selectedCategory = 'undefined';
                        switch (selectedCategoryLabel) {
                            case '1':
                                selectedCategory = 'noun';
                                break;

                            case '2':
                                selectedCategory = 'descr';
                                break;

                            case '3':
                                selectedCategory = 'verb';
                                break;
                        }
                        // console.log('selected:'+selectedCategory);
                        let keywordObj = {
                            "name": keywordText,
                            "category": selectedCategory,
                            "picsCount": 0
                        }
                        submitObj.keywords.push(keywordObj);
                    }
                    submitedKeywordsObjArr = submitObj.keywords;
                });
                // console.dir(submitObj);

                $.ajax({
                    type: "POST",
                    url: `${_webServiceUrl}pictures/insertUrl`,
                    data: JSON.stringify(submitObj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                }).then(function (msg) { sucessfullPost(msg); }, function (errMsg) {
                    if (errMsg.status === 201) {
                        // successful post
                        sucessfullPost(errMsg);
                    } else {
                        // failed post
                        console.log("failure : " + errMsg.responseText);
                        alert("failure : " + errMsg.responseText);
                    }
                });
            },
            Cancel: function () {
                $(this).dialog("close");
            },
            'Clear keywords': function () {
                $('.keyword-textbox').val('');
                $('.pics-count').each((idx, divElm) => { divElm.innerText = '' });
                $('input[type=radio]').each((idx, radioElm) => { radioElm.checked = false; });
                $('input[type=radio]').button('refresh');
            }
        }

    });
});

function loadRelatedKeywordsText(textInputElm, keywordObj) {
    // find the corect Div elment
    let relatedKeywordsDivElm = textInputElm.parentElement.parentElement.getElementsByClassName('related-keywords')[0];
    let relatedKeywordsStr = '';
    // read all the related keywords into string and assign to div elem
    keywordObj.relatedKeywords.forEach(function (item) {
        if (item.count > 1) {
            relatedKeywordsStr = relatedKeywordsStr + ` ${item.name}(${item.count})`;
        }
    });
    relatedKeywordsDivElm.innerText = relatedKeywordsStr;
}

function pupulateKeywordRow(textInputElm, keywordObj) {
    textInputElm.value = keywordObj.name;

    // identify category radio button
    let radioButtonIdx = 0;
    switch (keywordObj.category) {
        case "noun":
            radioButtonIdx = 1;
            break;

        case "descr":
            radioButtonIdx = 2;
            break;

        case "verb":
            radioButtonIdx = 3;
    }
    let row = textInputElm.id.slice(-3, -2);
    let radioButtonId = `#radio${row}-${radioButtonIdx}`;
    $(radioButtonId).click();
    let keywordCountDivId = `#keyword-count-${row}`;
    $(keywordCountDivId).html(keywordObj.picsCount);

    // populated related keyword if there are any
    if (!keywordObj.hasOwnProperty('relatedKeywords')) {
        $.getJSON(`${_webServiceUrl}relatedKeywordses/search/findRelatedByName?name=${keywordObj.name}`)
        .done(function (resp){
            let relatedKeywordsObjArr = resp._embedded.relatedKeywordses;
            keywordObj.relatedKeywords = relatedKeywordsObjArr;
            loadRelatedKeywordsText(textInputElm, keywordObj);
        });
        return;
    };
    if (keywordObj.relatedKeywords.length == 0) return;
    loadRelatedKeywordsText(textInputElm, keywordObj);
}

// Add listner on CTRL-Q
$(document).on("keypress", function (e) {
    // console.log("keypressed with keycode: " + e.keyCode);
    /*ctrl+q*/
    if (e.keyCode == 17) {
        // $("<a>").attr("href", activeLink).attr("target", "_blank")[0].click();
        // console.info('CTRL-Q on img. on: ' + activeLink);
        $('#img_src').attr("src", activeImg);
        $('#img_href').attr("href", activeLink);
        let img_text = activeImg.substr(activeImg.lastIndexOf('?') + 1);
        $('#img_title').text(img_text);
        capturedImg = activeImg;


        // Clear previous rows, except first
        $('.imagemarker-dialog-section:first-of-type input').val("");
        while ($('.imagemarker-dialog-section').length > 1) {
            $('.imagemarker-dialog-section').last().remove();
        }
        $('.pics-count,.related-keywords').html('');


        // Rebuild all rows 
        for (let rowsCount = 2; rowsCount <= maxRowsCount; rowsCount++) {
            let newRow = $('.imagemarker-dialog-section:first-of-type').clone();
            newRow.html(newRow.html().replace(/radio1/g, "radio" + rowsCount));
            newRow.html(newRow.html().replace(/text1/g, "text" + rowsCount));
            newRow.html(newRow.html().replace(/radioset1/g, "radioset" + rowsCount));
            newRow.html(newRow.html().replace(/keyword-count-1/g, "keyword-count-" + rowsCount));
            newRow.find('SPAN').remove();
            $('.imagemarker-dialog-form').append(newRow);
        }
        $(".radioset").buttonset();
        // Insert current keywords info
        $('.imagemarker-dialog-section').each(function (row, element) {
            if (submitedKeywordsObjArr.length > 0 && row < submitedKeywordsObjArr.length) {
                let textInput = element.getElementsByTagName('input')[0];
                pupulateKeywordRow(textInput, submitedKeywordsObjArr[row]);
            }
        });
        // Attach autocomplete functionality to each textbox
        $(".keyword-textbox").autocomplete({
            source: availableKeywordsArr,
            select: function (event, ui) {
                let keywordIdx = availableKeywordsArr.indexOf(ui.item.value);
                let keywordObj = allKeywordsObjArr[keywordIdx];
                let textInputElm = $(`#${event.target.id}`)[0];
                pupulateKeywordRow(textInputElm, keywordObj);
            }
        });
        $(".keyword-textbox").blur((event) => {
            let currTargetElm = event.currentTarget;
            if (currTargetElm.value.length > 0) {
                // console.dir(event.currentTarget);
                // console.log(`current keyword is: ${currTargetElm.value}`);
                let keywordIdx = availableKeywordsArr.indexOf(currTargetElm.value);
                if (keywordIdx > -1) {
                    let keywordObj = allKeywordsObjArr[keywordIdx];
                    pupulateKeywordRow(currTargetElm, keywordObj);
                }
            }
        });
        // Finally open the dialog box and activate it for UI
        $("#dialog-1").dialog("open");
        // console.info('#dialog opened for: '+img_text);
    }
});

// Mouse listener for any move event on the current document.
document.addEventListener('mousemove', function (e) {
    let srcElement = e.srcElement;
    if (srcElement == null || srcElement.parentElement == null || srcElement.parentElement.className == null) {
        return;
    }
    let prntClass = srcElement.parentElement.className;

    // Lets check if our underlying element is a IMG.
    if (prevDOM != srcElement && srcElement.nodeName == 'IMG' && prntClass === 'tjGalleryItem') {

        // For NPE checking, we check safely. We need to remove the class name
        // Since we will be styling the new one after.
        if (prevDOM != null) {
            prevDOM.classList.remove(MOUSE_VISITED_CLASSNAME);
        }

        // Add a visited class name to the element. So we can style it.
        srcElement.classList.add(MOUSE_VISITED_CLASSNAME);

        // The current element is now the previous. So we can remove the class
        // during the next ieration.
        prevDOM = srcElement;
        activeImg = srcElement.currentSrc;
        // console.info(activeImg);

        // Locate the link in the grandparent element.
        let linkElement = srcElement.parentElement.parentElement;
        activeLink = linkElement.href;
        // console.info(activeLink);
    }
}, false);