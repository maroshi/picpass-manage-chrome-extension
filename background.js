'use strict';

var _webServiceUrl = 'https://localhost:8443/api/';

chrome.runtime.onInstalled.addListener(function () {
    console.log("extension installed");
    // Read all keywords to local storage
    $.getJSON(`${_webServiceUrl}keywordPicsCounts/search/all`,
        function (respData) {
            let keywordCountedObjsArr = respData._embedded.keywordPicsCounts;
            // console.dir(keywordCountedObjsArr);
            chrome.storage.local.set({ "keywordCountedObjsArr": keywordCountedObjsArr }, function () {
                console.dir(["Saved keywordCountedObjsArr", keywordCountedObjsArr]);
            });
        });

    // Initialize submitedKeywordsObjArr to empty array, and save it.
    let submitedKeywordsObjArr = [];
    chrome.storage.local.set({ "submitedKeywordsObjArr": submitedKeywordsObjArr }, function () {
        console.dir(["Saved submitedKeywordsObjArr", submitedKeywordsObjArr]);
    });
    


    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: { hostEquals: 'www.clipart.com' },
            })
            ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});